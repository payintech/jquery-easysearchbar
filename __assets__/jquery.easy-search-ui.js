(function ($) {
    var getLocale = function (lang) {
        var i18n = {
            fr: {
                labels: {
                    true: 'vrai',
                    false: 'faux',
                    remove_predicate: 'Supprimer le prédicat',
                    search: 'Filtrer'
                },
                operators: {
                    eq: 'est égal &agrave;',
                    like: 'est égal &agrave;',
                    ilike: 'est égal &agrave;',
                    contains: 'contient',
                    icontains: 'contient',
                    gt: 'est plus grand que',
                    gte: 'est égal ou plus grand que',
                    lt: 'est plus petit que',
                    lte: 'est égale ou plus petit que',
                    isnull: 'est NULL',
                    isnotnull: 'n\'est pas NULL',
                    startswith: 'commence par',
                    istartswith: 'commence par',
                    endswith: 'se termine par',
                    iendswith: 'se termine par',
                    in: 'est compris dans',
                    notin: 'n\'est pas compris dans'
                },
                months: [
                    'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'
                ],
                weekdays: [
                    'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'
                ],
                format: 'YYYY-MM-DD'
            }
        };
        return i18n[lang];
    };

    $.fn.searchBar = function (options) {
        /********** Public Settings ***********/
        if (options.i18n === undefined) {
            options.i18n = getLocale((options.lang !== undefined) ? options.lang : document.documentElement.lang);
        }
        var settings = $.extend({
            initialQuery: '',
            lang: document.documentElement.lang,
            i18n: {
                labels: {
                    true: 'true',
                    false: 'false',
                    remove_predicate: 'Remove predicate',
                    search: 'Search'
                },
                operators: {
                    eq: 'is equal to',
                    like: 'is equal to',
                    ilike: 'is equal to',
                    contains: 'contains',
                    icontains: 'contains',
                    gt: 'is greater than',
                    gte: 'is equal or greater than',
                    lt: 'is smaller than',
                    lte: 'is equal to or smaller than',
                    isnull: 'is NULL',
                    isnotnull: 'is not NULL',
                    startswith: 'starts with',
                    istartswith: 'starts with',
                    endswith: 'to end by',
                    iendswith: 'to end by',
                    in: 'is in',
                    notin: 'is not included in'
                }
            },
            models: [],
            quickSearch: []
        }, options);

        if (settings.framework) {
            settings.framework = settings.framework.toLowerCase();
        }

        if (!settings.framework || (settings.framework !== 'uikit' && settings.framework !== 'bootstrap')) {
            settings.framework = 'bootstrap';
        }

        /********** Private Settings & local attributes ***********/
        var $_container = this;

        var _prevQuickSearch = '';

        var _predicate = {
            operators: [
                'eq',
                'like',
                'ilike',
                'contains',
                'icontains',
                'gt',
                'gte',
                'lt',
                'lte',
                'isnull',
                'isnotnull',
                'startswith',
                'istartswith',
                'endswith',
                'iendswith',
                'in',
                'notin'
            ],
            pattern: /^([a-z_.]*)__(eq|like|ilike|contains|icontains|gt|gte|lt|lte|isnull|isnotnull|startswith|istartswith|endswith|iendswith|in|notin)(=.*)?$/i,
            types: {
                text: ['ilike', 'icontains', 'istartswith', 'iendswith'],
                boolean: ['eq'],
                number: ['eq', 'gt', 'gte', 'lt', 'lte', 'in', 'notin'],
                date: ['eq', 'gt', 'gte', 'lt', 'lte', 'isnull', 'isnotnull'],
                datetime: ['eq', 'gt', 'gte', 'lt', 'lte', 'isnull', 'isnotnull'],
                enum: ['eq', 'in', 'notin'],
                uuid: ['eq']
            }
        };

        var _locationSearch = document.location.search.slice(1);
        var _currentData = [];

        var contentProvider = {
            getIcon: function (name) {
                var icons = {
                    'validate': {'bootstrap': 'glyphicon glyphicon-ok', 'uikit': 'uk-icon-check'},
                    'remove': {'bootstrap': 'glyphicon glyphicon-remove', 'uikit': 'uk-icon-times'},
                    'plus': {'bootstrap': 'glyphicon glyphicon-plus', 'uikit': 'uk-icon-plus'},
                    'search': {'bootstrap': 'glyphicon glyphicon-search', 'uikit': 'uk-icon-search'},
                    'list': {'bootstrap': 'glyphicon glyphicon-th-list', 'uikit': 'uk-icon-list'}
                };
                return (icons[name] && icons[name][settings.framework]) ? icons[name][settings.framework] : '';
            },
            getButtonClass: function (name) {
                var buttons = {
                    'success': {'bootstrap': 'btn btn-success', 'uikit': 'uk-button uk-button-success'},
                    'primary': {'bootstrap': 'btn btn-primary', 'uikit': 'uk-button uk-button-primary'},
                    'danger': {'bootstrap': 'btn btn-danger', 'uikit': 'uk-button uk-button-danger'}
                };
                return (buttons[name] && buttons[name][settings.framework]) ? buttons[name][settings.framework] : '';
            },
            getHtmlElement: function (name, model, AdditionalValue) {
                if (!model) model = {};
                var elements = {
                    'htmlDatePicker': {
                        'bootstrap': '<input style="display: inline; width: auto" class="pit-searchBar-item-value form-control calendar-date ' + AdditionalValue + '" name="' + model.id + '" placeholder="' + model.label + '" type="text" autocomplete="off">',
                        'uikit': '<div class="uk-form-icon"><i class="uk-icon-calendar"></i><input class="uk-datepicker ' + AdditionalValue + '" name="' + model.id + '" type="text"/></div>'
                    },
                    'htmlInputTextMulti': '<div class="' + (settings.framework === 'bootstrap' ? 'form-control' : '') + '"><i class="' + contentProvider.getIcon('list') + '"></i><input name="' + model.id + '" type="text" value="" placeholder="' + model.label + ', ' + model.label + '" autocomplete="off"/></div>',
                    'htmlInputTextSingle': '<input style="display: inline; width: auto" class="pit-searchBar-item-value ' + (settings.framework === 'bootstrap' ? 'form-control' : '') + '" name="' + model.id + '" type="text" placeholder="' + model.label + '" autocomplete="off"/>',
                    'htmlInputBoolean': '<select class="' + (settings.framework === 'bootstrap' ? 'form-control' : '') + ' pit-searchBar-item-value" name="' + model.id + '"><option value="true">' + settings.i18n.labels.true + '</option><option value="false">' + settings.i18n.labels.false + '</option></select>',
                    'htmlInputNumberMulti': '<input style="display: inline; width: auto" class="pit-searchBar-item-value form-control" name="' + model.id + '" type="text" placeholder="num, num" autocomplete="off"/>',
                    'htmlInputNumberSingle': '<input style="display: inline; width: auto" class="pit-searchBar-item-value form-control" name="' + model.id + '" type="number" placeholder="' + model.label + '" autocomplete="off"/>',
                    'htmlInputEnumSelect': '<select name="select" class="' + (settings.framework === 'bootstrap' ? 'bootstrap' : 'uikit') + ' pit-searchBar-item-value select2" style="width:auto" name="' + model.id + '" ' + AdditionalValue + '></select>',
                    'htmlInputEnumOption': '<option value="' + AdditionalValue + '">' + (!model.values ? "" : model.values[AdditionalValue]) + '</option>',
                    'htmlInputUuid': '<input style="display: inline; width: auto" class="pit-searchBar-item-value ' + (settings.framework === 'bootstrap' ? 'form-control' : '') + ' formatter-uuid" name="' + model.id + '" type="text" placeholder="' + model.label + '" autocomplete="off"/>',
                    'htmlInputDefault': '<input style="display: inline; width: auto" class="pit-searchBar-item-value ' + (settings.framework === 'bootstrap' ? 'form-control' : '') + '" name="' + model.id + '" type="text" autocomplete="off"/>',
                    'htmlButtonSearch': '<button title="' + settings.i18n.labels.search + '" data-uk-tooltip class="' + contentProvider.getButtonClass('primary') + ' pit-searchBar-buttonSearch float-right"><i class="' + contentProvider.getIcon('search') + '"></i></button>',
                    'htmlButtonNewPredicate': '<button class="' + contentProvider.getButtonClass('primary') + ' ' + (settings.hideAdvancedSearch ? 'hidden' : '') + ' pit-searchBar-buttonAdd"><i class="' + contentProvider.getIcon('plus') + '"></i></button>'
                };
                return (elements[name] && elements[name][settings.framework]) ? elements[name][settings.framework] : elements[name];
            }
        };

        /********** Private methods ***********/

        var getModelByField = function (field) {
            return $.grep(settings.models, function (e) {
                return e.id == field;
            }).shift();
        };

        var parseQuery = function () {
            var query = settings.initialQuery.replace('?', '').split('&');
            if ($.inArray("qs=1", query) <= -1) {
                query.forEach(function (param) {
                    var predicate = queryToPredicate(param);
                    if (predicate !== null) {
                        $_container.append(getPredicate_UI(predicate));
                        _currentData.push(predicate);
                        var pattern = new RegExp('&?' + predicate.query);
                        _locationSearch = _locationSearch.replace(pattern, '');
                    }
                });
            } else {
                query.forEach(function (param) {
                    var predicate = queryToPredicate(param);
                    if (predicate !== null) {
                        _prevQuickSearch = param.split('=')[1];
                        var pattern = new RegExp('&?' + predicate.query);
                        _locationSearch = _locationSearch.replace(pattern, '');
                    }
                });
            }
        };

        var generateQuery = function () {
            var queryArgs = [];

            if (_currentData.length === 0) {
                var value = $_container.find('.pit-searchBar-qs').val();

                if (value !== null && value !== '') {
                    if (settings.quickSearch != null && settings.quickSearch.length > 0) {
                        queryArgs.push(settings.quickSearch[0].id + "__" + settings.quickSearch[0].operator + "=" + encodeURIComponent(value) + "&qs=1");
                    } else {
                        queryArgs.push("name__icontains=" + encodeURIComponent(value) + "&qs=1");
                    }
                }
            } else {
                _currentData.forEach(function (data) {
                    queryArgs.push(data.query);
                });
            }
            return queryArgs.join('&');
        };

        var isNonValueOperator = function (operator) {
            return ("isnull;isnotnull;".indexOf(operator) > -1);
        };

        var isMultiValueOperator = function (operator) {
            return ("in;notin;".indexOf(operator) > -1);
        };

        var parseDate = function (string) {
            var m = moment(string, "YYYY-MM-DD");
            m.utc();
            var d = new Date(m.format());
            if (d != 'Invalid Date') {
                return d;
            } else {
                return null;
            }
        };

        var parseDateTime = function (string) {
            var m = moment(string, "YYYY-MM-DD HH:mm");
            m.utc();
            var d = new Date(m.format());
            if (d != 'Invalid Date') {
                return d;
            } else {
                return null;
            }
        };

        var parseDateToLocal = function (string) {
            var m = moment.utc(string, "YYYY-MM-DD");
            var d = new Date(m.format());
            if (d != 'Invalid Date') {
                return d;
            } else {
                return null;
            }
        };

        var parseDateTimeToLocal = function (string) {
            var m = moment(string);
            var d = new Date(m.format());
            if (d != 'Invalid Date') {
                return d;
            } else {
                return null;
            }
        };

        var predicateToQuery = function (predicate) {
            var query = predicate.model.id + '__' + predicate.operator;
            if (!isNonValueOperator(predicate.operator)) {
                var value = predicate.value;
                if (value instanceof Array) {
                    value = value.join(',');
                } else if (value instanceof Date) {
                    if (predicate.model.type.toLowerCase() === 'date') {
                        value = moment(value).format('YYYY-MM-DD');
                    } else if (predicate.model.type.toLowerCase() === 'datetime') {
                        value = moment(value).format('YYYY-MM-DDTHH:mm');
                    }
                }
                query += '=' + encodeURI(value.trim());
            }
            return query;
        };

        var queryToPredicate = function (param) {
            var results = _predicate.pattern.exec(param);
            if (results === null || results.length != 4) {
                return null;
            }
            var model = getModelByField(results[1]);
            if (model === null || model === undefined) {
                return null;
            }
            var predicate = {
                query: results[0],
                model: model,
                operator: results[2],
                value: (results[3] === undefined) ? null : decodeURI(results[3].slice(1))
            };
            if (predicate.model.type.toLowerCase() === 'date') {
                predicate.value = parseDateToLocal(predicate.value);
            } else if (predicate.model.type.toLowerCase() === 'datetime') {
                predicate.value = parseDateTimeToLocal(predicate.value);
            } else if (isMultiValueOperator(predicate.operator)) {
                predicate.value = predicate.value.replace('[', '').replace(']', '').trim().split(',');
            }
            if (!isNonValueOperator(predicate.operator) && predicate.value === null) {
                return null;
            }
            return predicate;
        };

        var getPredicate_UI = function (predicate) {
            var ui =
                '<span class="pit-searchBar-item">' +
                '<span class="pit-searchBar-item-field">' + predicate.model.label + '</span>&nbsp;' +
                '<span class="pit-searchBar-item-operator">' + settings.i18n.operators[predicate.operator] + '</span>&nbsp;';

            if (!isNonValueOperator(predicate.operator)) {
                var value = predicate.value;
                if (isMultiValueOperator(predicate.operator) && value instanceof Array) {
                    if (predicate.model.type.toLowerCase().toLowerCase() == 'enum') {
                        value = [];
                        predicate.value.forEach(function (k) {
                            value.push(predicate.model.values[k]);
                        });
                    }
                    value = '[' + value.join(", ") + ']';
                } else {
                    if (predicate.model.type.toLowerCase().toLowerCase() == 'enum') {
                        value = predicate.model.values[value];
                    }
                }
                switch (predicate.model.type.toLowerCase()) {
                    case 'boolean':
                        ui += '<span class="pit-searchBar-item-boolean">' + settings.i18n.labels[value.toLocaleLowerCase()] + '</span>';
                        break;
                    case 'number':
                        ui += '<span class="pit-searchBar-item-number">' + value + '</span>';
                        break;
                    case 'date':
                        ui += '<span class="pit-searchBar-item-date">[' + moment(value).format('DD/MM/YYYY') + ']</span>';
                        break;
                    case 'datetime':
                        ui += '<span class="pit-searchBar-item-date">[' + moment(value).format('DD/MM/YYYY') + ' ' +  moment(value).format('HH:mm') + ']</span>';
                        break;
                    case 'enum':
                        ui += '<span class="pit-searchBar-item-enum">' + value + '</span>';
                        break;
                    case 'uuid':
                        ui += '<span class="pit-searchBar-item-uuid">' + value + '</span>';
                        break;
                    default:
                        ui += '<span class="pit-searchBar-item-string">"' + value + '"</span>';
                }
            }

            ui += '<span class="pit-searchBar-item-remove" title="' + settings.i18n.labels.remove_predicate + '" data-uk-tooltip data-search-raw="' + predicate.query + '"> <i class="' + contentProvider.getIcon('remove') + '"></i></span> ' +
                '</span> ';

            return ui;
        };

        var getPredicateForm_UI = function () {
            var ui =
                '<div class="pit-searchBar-newPredicat uk-form">' +
                '<fieldset data-uk-margin>' +
                '<span class="pit-searchBar-field">' +
                '<select class="' + (settings.framework === 'bootstrap' ? 'form-control' : '') + ' pit-searchBar-selectOne">';
            for (var i = 0; i < settings.models.length; ++i) {
                ui += '<option value="' + i + '">' + settings.models[i].label + '</option>';
            }
            ui += '</select> ' +
                '</span> ' +
                '<span class="pit-searchBar-operator"></span> ' +
                '<span class="pit-searchBar-value"></span> ' +
                '<button type="button" class="' + contentProvider.getButtonClass('success') + ' pit-searchBar-item-add"><i class="' + contentProvider.getIcon('validate') + '"></i></button> ' +
                '<button type="button" class="' + contentProvider.getButtonClass('danger') + ' pit-searchBar-item-cancel"><i class="' + contentProvider.getIcon('remove') + '"></i></button>' +
                '</fieldset>' +
                '</div>';
            return ui;
        };

        var getQuickSearch_UI = function () {
            if ($('.pit-sarchBar-qs').length > 0 || $('.pit-searchBar-newPredicat').length > 0)
                return '';
            if (_prevQuickSearch === null || _prevQuickSearch === '')
                return '<input type="text" placeholder="Quick Search" autocomplete="off" class="pit-searchBar-qs ' + (settings.hideQuickSearch ? 'hidden' : '') + ' "/>';
            else
                return '<input type="text" value="' + decodeURIComponent(_prevQuickSearch) + '" autocomplete="off" class="pit-searchBar-qs ' + (settings.hideQuickSearch ? 'hidden' : '') + '"/>';
        };

        var getPredicateValue_UI = function (model, operator) {
            switch (model.type.toLocaleLowerCase()) {
                //UI for simple text select
                case "text":
                    if (isMultiValueOperator(operator)) {
                        return contentProvider.getHtmlElement('htmlInputTextMulti', model);
                    } else {
                        return contentProvider.getHtmlElement('htmlInputTextSingle', model);
                    }
                    break;
                //UI for true/false select
                case "boolean":
                    return contentProvider.getHtmlElement('htmlInputBoolean', model);
                //UI for basic number select
                case "number":
                    if (isMultiValueOperator(operator)) {
                        return contentProvider.getHtmlElement('htmlInputNumberMulti', model);
                    } else {
                        return contentProvider.getHtmlElement('htmlInputNumberSingle', model);
                    }
                    break;
                //UI for Date (just date) select
                case "date":
                    return contentProvider.getHtmlElement('htmlDatePicker', model);
                //UI for Date and time select
                case "datetime":
                    return contentProvider.getHtmlElement('htmlDatePicker', model, 'datetime');
                //UI for Enum (list of values predefined)
                case "enum":
                    var option = isMultiValueOperator(operator) ? 'multiple' : '';
                    var $ui = $(contentProvider.getHtmlElement('htmlInputEnumSelect', model, option));
                    var values = Object.keys(model.values);
                    values.forEach(function (k) {
                        $ui.append(contentProvider.getHtmlElement('htmlInputEnumOption', model, k));
                    });
                    return $ui;
                //UI for UUID
                case "uuid":
                    return contentProvider.getHtmlElement('htmlInputUuid', model);
                //Default UI if data type unknown
                default:
                    return contentProvider.getHtmlElement('htmlInputDefault', model);
            }
        };

        var getOperatorsList_UI = function (model) {
            if (!_predicate.types[model.type.toLowerCase()])
                model.type = 'text';
            var operators = _predicate.types[model.type.toLowerCase()];
            var ui = '<select class="form-control pit-searchBar-selectOne"' + (operators.length <= 1 ? 'disabled' : '') + '>';
            operators.forEach(function (operator) {
                ui += '<option value="' + operator + '">' + settings.i18n.operators[operator] + '</option>';
            });
            ui += '</select> ';

            return ui;
        };

        var launchQuickSearch = function () {
            var url = generateQuery();
            if (_locationSearch != "") {
                _locationSearch += "&";
            }
            var new_url = $_container.urlCleaner({'url':  url, 'predicate_pattern': _predicate.pattern, 'settings': settings});
            document.location.href = new_url;
        };

        var submitItemSearch = function () {
            if (settings.framework === 'bootstrap')
                var value = $_container.find('.pit-searchBar-newPredicat .pit-searchBar-value select.pit-searchBar-item-value, .pit-searchBar-newPredicat .pit-searchBar-value input.pit-searchBar-item-value').val();
            else
                var value = $_container.find('.pit-searchBar-newPredicat .pit-searchBar-value input, .pit-searchBar-newPredicat .pit-searchBar-value select').val();
            var field = $_container.find('.pit-searchBar-newPredicat .pit-searchBar-field select').val();
            var operator = $_container.find('.pit-searchBar-newPredicat .pit-searchBar-operator select').val();

            if (value === '' && !isNonValueOperator(operator) || value === null) {
                return false;
            }
            var model = settings.models[field];
            if (model.type.toLowerCase() == 'date') {
                value = parseDate(value);
            }
            if (model.type.toLowerCase() == 'datetime') {
                value = parseDateTime(value);
            }
            if (model.type == 'number' && isMultiValueOperator(operator) && !(value instanceof Array)) {
                value = value.replace(' ', '').split(/[\s,]+/);
                value.forEach(function (e) {
                    e = e.replace(/\s+/g, '');
                })
            }
            else if (isMultiValueOperator(operator) && !(value instanceof Array)) {
                value = value.replace(' ', '').split(',');
            }
            var predicate = {
                model: model,
                operator: operator,
                value: value
            };
            predicate.query = predicateToQuery(predicate);
            $_container.append(getPredicate_UI(predicate));
            _currentData.push(predicate);

            $_container.find('.pit-searchBar-newPredicat').remove();
            $_container.append(contentProvider.getHtmlElement('htmlButtonNewPredicate'));
            $_container.trigger('search-item-update');
            $_container.trigger('search-ui-update');

            $('.pit-searchBar-buttonSearch').focus();
        };

        /********** Initialize ***********/
        parseQuery(this);
        this.append(contentProvider.getHtmlElement('htmlButtonNewPredicate'));
        if (_currentData.length === 0) {
            $_container.append(getQuickSearch_UI());
        }
        this.append(contentProvider.getHtmlElement('htmlButtonSearch'));

        /********** Callback / Event listener ***********/
        this.on('click', '.pit-searchBar-item-remove', function () {
            var raw = $(this).data('search-raw');
            if (raw !== undefined) {
                for (var i = 0; i < _currentData.length; ++i) {
                    if (_currentData[i].query == raw) {
                        _currentData.splice(i, 1);
                        $(this).closest('.pit-searchBar-item').remove();
                    }
                }
            }
            if (_currentData.length === 0) {
                $_container.append(getQuickSearch_UI());
            }
            $_container.trigger('search-item-update');
        });

        this.on('click', '.pit-searchBar-item-add', function () {
            submitItemSearch();
        });

        this.on('click', '.pit-searchBar-buttonSearch', function () {
            launchQuickSearch();
        });

        this.on('click', '.pit-searchBar-item-cancel', function () {
            $_container.find('.pit-searchBar-newPredicat').remove();
            $_container.append(contentProvider.getHtmlElement('htmlButtonNewPredicate'));
            if (_currentData.length === 0) {
                $_container.append(getQuickSearch_UI());
            }
            $_container.trigger('search-ui-update');
        });

        this.on('click', '.pit-searchBar-buttonAdd', function () {
            $(this).remove();
            $_container.append(getPredicateForm_UI());
            $_container.find('.pit-searchBar-qs').remove();
            $_container.find('.pit-searchBar-field  .pit-searchBar-selectOne').trigger('change', {container: $_container});
            $_container.trigger('search-ui-update');
        });

        this.on('change', '.pit-searchBar-field .pit-searchBar-selectOne', function () {
            var model = settings.models[$(this).val()];
            $_container.find('.pit-searchBar-newPredicat .pit-searchBar-operator').html(getOperatorsList_UI(model));
            $_container.find('.pit-searchBar-newPredicat .pit-searchBar-value').html(getPredicateValue_UI(model));
            $_container.trigger('search-ui-update');
            //TODO: Remove the following line if not used
            //$('.select2').select2();
            $('.select2-selection').addClass(settings.framework);
        });

        this.on('change', '.pit-searchBar-operator .pit-searchBar-selectOne', function () {
            var operator = this.value;
            var model = settings.models[$_container.find('.pit-searchBar-field  .pit-searchBar-selectOne').val()];
            if (isNonValueOperator(operator)) {
                $_container.find('.pit-searchBar-value').hide();
            } else {
                $_container.find('.pit-searchBar-value').show();
            }
            if (isMultiValueOperator(operator)) {
                $_container.find('.pit-searchBar-newPredicat .pit-searchBar-value').html(getPredicateValue_UI(model, operator));
            } else {
                $_container.find('.pit-searchBar-newPredicat .pit-searchBar-value').html(getPredicateValue_UI(model, operator));
            }
            $_container.trigger('search-ui-update');
            if (jQuery().select2) {
                $('select.' + settings.framework + '[multiple]').select2();
            } else {
                $('select.' + settings.framework + '[multiple]').multiSelect(settings.framework, settings.lang);
            }
            $('.select2-selection').addClass(settings.framework);
        });

        this.on('search-ui-update', function () {
            if (settings.framework === 'uikit' && $('.uk-datepicker').length > 0) {
                $.UIkit.datepicker($('.uk-datepicker'), {
                    format: 'YYYY-MM-DD',
                    weekstart: 0,
                    i18n: {
                        "months": settings.i18n.months,
                        "weekdays": settings.i18n.weekdays
                    }
                });
                $.UIkit.datepicker($('.uk-datepicker.datetime'), {
                    format: 'YYYY-MM-DD HH:mm',
                    weekstart: 0,
                    i18n: {
                        "months": settings.i18n.months,
                        "weekdays": settings.i18n.weekdays
                    }
                });
            }
            if ($('.calendar-date.datetime').length > 0) {
                $('.calendar-date').datetimepicker({
                    useCurrent: false,
                    locale: settings.lang,
                    format: 'YYYY-MM-DD HH:mm'
                });
            } else if ($('.calendar-date').length > 0) {
                $('.calendar-date').datetimepicker({
                    useCurrent: false,
                    locale: settings.lang,
                    format: 'YYYY-MM-DD',
                    enabledHours: false
                });
            }


            $.fn.formatter.addInptType('H', /[0-9a-fA-F]/)
            $(".formatter-uuid").formatter({
                'pattern': '{{HHHHHHHH}}-{{HHHH}}-{{HHHH}}-{{HHHH}}-{{HHHHHHHHHHHH}}',
                'persistent': false
            });
        });

        this.on('keydown', '.pit-searchBar-qs', function (e) {
            if (e.keyCode == 13) {
                launchQuickSearch();
            }
        });

        this.on('keyup', '.pit-searchBar-item-value', function (e) {
            if (e.keyCode == 13) {
                submitItemSearch();
            }
        });

        return this;
    };

    $.fn.urlCleaner = function (options) {

        if ( !options.settings || !options.predicate_pattern) {
            return (document.location.href.split('?')[0]);
        }

        var new_arguments = options.url,
            current_url = document.location.href.split('?'),
            predicate_pattern = options.predicate_pattern,
            ignorePattern = options.settings.ignorePattern,
            new_url = '';

        /* Useful if some arguments contains a '?' (for example for a text query) */
        if (current_url.length > 2) {
            for (var i = 2; i < current_url.length; i++) {
                current_url[1] += current_url[i];
            }
            current_url.splice(2);
        }
        if (current_url.length <= 1) {
            new_url = current_url[0] + '?' + new_arguments;
        } else {
            new_url = current_url[0] + '?';
            var current_arguments = current_url[1].split('&');
            current_arguments.forEach(function (e, i) {
                if (!predicate_pattern.test(e) && e !== 'qs=1') {
                    /* Looking for ignore pattern rules */
                    var patternFind = false;
                    if (ignorePattern) {
                        ignorePattern.forEach(function (_ignorePattern) {
                            var _pattern = new RegExp(_ignorePattern.pattern);
                            if (_pattern && _pattern.test(e)) {
                                new_url += (_ignorePattern.default ? _ignorePattern.default + '&' : '');
                                patternFind = true;
                            }
                        });
                    }
                    if (!patternFind) {
                        new_url += e + '&';
                    }
                }
            });
            new_url += new_arguments;
            if (new_url.substr(new_url.length - 1) == '&') {
                new_url.slice(0, -1);
            }
        }

        if (new_url[new_url.length - 1] == '&') {
            new_url = new_url.slice(0, -1);
        }
        if (new_url[new_url.length - 1] == '?') {
            new_url = new_url.slice(0, -1);
        }
        return new_url;
    }
}(jQuery));
