"use strict";
jQuery.fn.extend({
    multiSelect: function (theme, locale) {
        var i18n = {
            fr: {
                empty: "Selectionner..."
            },
            en: {
                empty: "Select..."
            }
        };
        var lang = (i18n[locale] ? i18n[locale] : 'en');

        var ui = {
            bootstrap: {
                dropDown: "<div id='{ID}' class='dropdown'><button class='btn btn-default dropdown-toggle' type='button' id='{RANDOM}' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><span class='value'></span><span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='{RANDOM}'></ul></div>",
                iconChecked: "glyphicon glyphicon-check",
                iconUnchecked: "glyphicon glyphicon-unchecked"
            },
            uikit: {
                dropDown: "<div id='{ID}' class='uk-button-dropdown' data-uk-dropdown='{mode:\"click\"}''><button class='uk-button uk-multiselect-button'><span class='value'></span> <i class='uk-icon-caret-down'></i></button><div class='uk-dropdown'><ul class='uk-nav uk-nav-dropdown'></ul></div></div>",
                iconChecked: "uk-icon-check-square-o",
                iconUnchecked: "uk-icon-square-o"
            }
        };

        if(theme === null || theme === undefined) {
            return;
        }
        theme = theme.toLowerCase();
        if(ui[theme] === undefined) {
            console && console.warn("UI [" + theme + "] is not initialize !");
            return;
        }

        var $select = this;

        //Define a unique id if not set by user
        var id = $select.attr("id");
        if (id === undefined) {
            id = Math.random().toString(36).substring(7);
            $select.attr('id', id);
        }
        //Create mselect component.
        $select
            .hide()
            .after(ui[theme].dropDown.replace(/\{ID\}/g, id + "-mselect").replace(/\{RANDOM\}/g, id + "-bs"));

        var $mSelect = $select.next("#" + id + "-mselect");
        if(theme === "bootstrap") {
            $mSelect.find("ul").on('click', function () {
                event.stopPropagation();
            });
        }

        var updateTitle = function() {
            var str = $select.find("option:selected").map(function() {
                return $(this).text();
            }).get().join(", ");
            if (str.length == 0) {
                str = lang.empty;
            }
            var title = str.substring(0, 20);
            if(str.length > 20) {
                title += "...(" + $select.find("option:selected").length + ")";
            }
            $mSelect.find('.value').html(title).attr("title", str);

        };
        updateTitle();

        //Construct mselect options list
        var htmlOptions = "";
        $select.find("option").each(function() {
            var $opt = $(this);
            if($opt.prop("selected")) {
                htmlOptions += "<li class='checked' data-value='" + $opt.val() + "'><a><i class='" + ui[theme].iconChecked + "'></i> &nbsp; " + $opt.html() + "</a></li>";
            } else {
                htmlOptions += "<li data-value='" + $opt.val() + "'><a><i class='" + ui[theme].iconUnchecked + "'></i> &nbsp; " + $opt.html() + "</a></li>";
            }
        });
        $mSelect.find("ul").html(htmlOptions);

        $mSelect.find("li").on('click', function() {
            var $opt = $(this);
            $opt.find("i").toggleClass(ui[theme].iconChecked + " " + ui[theme].iconUnchecked);
            $opt.toggleClass("checked");
            $select.find("option[value='" + $opt.data('value') + "']").prop("selected", function(selected) {
                return !selected;
            }).attr('selected', function(_, attr) {
                return !attr;
            });
            updateTitle();
        });
    }
});
