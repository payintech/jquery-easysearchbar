# jQuery easySearchUI

[![Latest release](https://img.shields.io/badge/latest_release-16.08-orange.svg)](https://github.com/payintech/jquery-easy-search-ui/releases)
[![Build](https://img.shields.io/travis-ci/payintech/jquery-easy-search-ui.svg?branch=master&style=flat)](https://travis-ci.org/payintech/jquery-easy-search-ui)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/payintech/jquery-easy-search-ui/master/LICENSE)


easySearchUI is a simple jQuery plugin to add a simple graphical search bar on your website.
*****


## Questions and issues
The GitHub issue tracker is only for bug reports and feature requests. Anything
else, such as questions for help in using the library, should be posted at
[StackOverflow](http://stackoverflow.com/questions/tagged/easy-search-ui?sort=active)
under tags `easy-search-ui` and `jquery`.




## Build the webjar
To compile s-money-client, you must ensure that Java 7 and Maven are correctly
installed and are functional.

    #> mvn package




## Usage

### Play Framework 2.5.x

```html
<script src="@routes.Assets.versioned("lib/webjar-easysearchui/jquery.easy-search-ui.min.js")"></script>
```
### General usage

#### 1) Include the plugin's files to you project

The plugin works with both Bootstrap and Uikit frameworks. All you have to do is to specifie which one you are currently using on you website.

First, include that two files:
```html
<link rel="stylesheet/less" type="text/css" href="../__assets__/jquery.easy-search-ui.less"/>
<script src="../__assets__/jquery.easy-search-ui.js"></script>
```

#### 2) Include the dependencies

Required files: 

    - jquery.js
    - moment.js (prefere moment-with-locales.js)
    - formater.js

Required files to use it with Bootstrap: 

    - Bootstrap.js
    - Bootstrap.css
    - bootstrap-datetimepicker.js (only if you want to use the searchBar with dates requests)
    - bootstrap-datetimepicker.min.css

Required files to use it with UIkit: 

    - uikit.js (with datepicker_
    - uikit.css (with datepicker)

#### 3) Insert the searchBar using the 'pit-searchBar-container' id

```html
<div id="pit-searchBar-container"></div>
```

(Note that you can only insert one searchBar per page)

#### 4) Configure your searchBar using Javascript

```javascript
$(document).ready(function () {
        $("#pit-searchBar-container").searchBar({ //Initialize the SearchBar
            initialQuery: window.location.search,
            framework: 'uikit', //define the Framework you want to use (default: bootstrap)
            i18n: 'fr', //Define witch locale to use (actually, only french and english are supported). If not defined, it will load the locale from the user browser
            hideAdvancedSearch: false, // Optional field that allow you to specifie if you want to hide the advanced search tool button
            models: [ //Here are the differents fields you can add to the advanced search tool
                {type: 'date', id: 'date', label: 'date'},
                {type: 'boolean', id: 'bool', label: 'bool'},
                {type: 'number', id: 'num', label: 'num'},
                {type: 'enum', id: 'enum', label: 'enum', values: {'SUPER':'super', 'MEGA': 'mega', 'GIGA': 'giga', 'SUPRA': 'supra'}},
                {type: 'text', id: 'text', label: 'text'},
                {type: 'uuid', id: 'uuid', label: 'uuid'}
            ]
        });
    });
```

As you can see, each field requires three informations:
* The **type** defines the format expected
* The **id** will be used in the GET request when the request is submitted
* The **label** will be used inside the searchbar as the name of the field
    
There are several differents types you can use:

| Name          | Description   | Comparators working with  |
| ------------- |-------------|-----|
| Date | Make a search from a date using a datePicker | equal to, greater than, equal to or greater than, smaller than, equal to or smaller than, NULL, not NULL |
| Boolean | Check if the value is true or false | equal to |
| Number | Make a search from an integer value | equal to, greater than, equal to or greater than, smaller than, equal to or smaller than, in, not included in |
| Enum | Make a search from a list of propositions | equal to, in, not included in |
| Text | Make a search from a text sample | equal to, contains, start with, end by |
| Uuid | Look for an uuid | equal to |

#### 5) Enjoy!

You are done with configuration, your searchBar is now ready to use. But don't forget that you'll need some integrations on the backside of your website!

### How it works

We are gonna use a **concrete example** to see how it works.


Let's say you have an array on your website with a lot of data but you **only want to show the entries with a date that is greater than 2016-05-02**.

First, **open the advanced search tool** by clicking on the plus button on the left. Then, **select the field that interest you**, in our case it will be the date. This field has been first defined by yourself when you added the search bar.


You'll now have the choice between **several comparators**, in this example we will use the 'is greater than' one. All you need to do next is to specifie the targeted date.

Now, **click on the search button** on the right, it'll reload the page with a **GET request** that will appears in your brower's URL.

That URL will looks like this:
```html
http://localhost/jquery-easysearchbar/__demo__/uikit_demo.html?date__gt=2016-05-02
```

As you can see, **date__gt=2016-05-02** matches the request. **'date'** is the requiered field, **'gt'** means that you need a **'greater than'** response and **'2016-05-02'** is the target date.

Of course, it's steel your job to get this URL and to actualize the content of the page. 

**nb:** Note that you can **mix different fields** to make your request be more relevant.

## Demo

You'll find a demo in the ___demo___ folder. It shows you how to include the searchBar to your project and how to use it with Bootstrap or UIkit.

## TODOS

* Make the searchBar working with Ajax
* Make the enum multi-select more efficient
* Remove the page parameters in the URL when the searchBar is reset
* Add new language support
* Find a way to make the advanced search tool working on small screens (actually disabled for esthetics reasons)

## Notes

This project is using LESS. If you don't know how to compile it on your server, you can include that file to your page. It will compile less files using Javascript. Note that this method is only recommended for development purpose:

``` html
<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.min.js"></script>
```
If you do not wan't to use LESS, you can edit the LESS file, there is no big modifications to make in it.

#### multi-select

This plugin has been developed for this project. It is not essential to use it but hardly recommended. The multi-select has been optimized to works with both Bootstrap and UIkit frameworks.
